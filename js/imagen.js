document.getElementById('fileInput').addEventListener('change', function(event) {
    const file = event.target.files[0];
    
    if (file) {
        const reader = new FileReader();
        
        reader.onload = function(e) {
            const imageOutput = document.getElementById('imageOutput');
            const image = document.createElement('img');
            image.src = e.target.result;
            image.alt = "Imagen cargada";
            
            imageOutput.innerHTML = '';
            imageOutput.appendChild(image);
        };

        reader.readAsDataURL(file);
    }
});