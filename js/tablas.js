const imagenes = {
    0: '/img/tablas/0.png',
    1: '/img/tablas/1.png',
    2: '/img/tablas/2.png',
    3: '/img/tablas/3.png',
    4: '/img/tablas/4.png',
    5: '/img/tablas/5.png',
    6: '/img/tablas/6.png',
    7: '/img/tablas/7.png',
    8: '/img/tablas/8.png',
    9: '/img/tablas/9.png',
    10: '/img/tablas/10.png',
    'x': '/img/tablas/x.png',
    '=': '/img/tablas/=.png'
  };
  
function mostrarTabla() {
    const numeroSeleccionado = document.getElementById('selectorNumero').value;

    const divResultados = document.getElementById('tablaResultados');
    divResultados.innerHTML = '';

    for (let i = 1; i <= 10; i++) {
        const resultado = numeroSeleccionado * i;
        const imgNumSel = crearImagen(numeroSeleccionado);
        const imgI = crearImagen(i);
        const imgsResultado = obtenerImagenesParaNumero(resultado);
        const imgMultiplicacion = document.createElement('img');
        imgMultiplicacion.src = imagenes['x'];
        const imgIgual = document.createElement('img');
        imgIgual.src = imagenes['='];

        const p = document.createElement('p');
        p.appendChild(imgNumSel);
        p.appendChild(imgMultiplicacion);
        p.appendChild(imgI);
        p.appendChild(imgIgual);

        imgsResultado.forEach(img => p.appendChild(img));

        divResultados.appendChild(p);
    }
}

/** 
 * @param {number} numero - El número para el cual se debe obtener la imagen correspondiente.
 * @return {HTMLImageElement} - El elemento de imagen (<img>) creado.
 */

function crearImagen(numero) {
    const img = document.createElement('img');
    img.src = imagenes[numero];
    return img;
}

  function obtenerImagenesParaNumero(numero) {
    const digitos = Array.from(String(numero), Number);
    return digitos.map(digito => crearImagen(digito));
  }
  
  document.getElementById('btnMostrarTabla').addEventListener('click', mostrarTabla);

  
  